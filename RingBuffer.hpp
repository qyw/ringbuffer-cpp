/**
 * \author 	Ivan "Kyb Sledgehammer" Kuvaldin <i.kyb[2]ya,ru>
 * \brief   
 *			Реализация кольцевого буффера с расширенными возможностями.
 */
 
/** Define to prevent recursive inclusion -----------------------------------*/
#ifndef _Sledge_RingBuffer_hpp_
#define _Sledge_RingBuffer_hpp_


#pragma diag_suppress 284  // Suppress null-reference is not allowed. Exceptions are not good solution for uC


//#include "Buffer.hpp"
#include "Sledge/utils.h"
#include "Sledge/assert.h"

#include <cstddef>
using std::size_t;


namespace Sledge {

	/**	
	 * Шаблон кольцевого буфера
	 * принимает два параметра:
	 * @param SIZE: 	размер буфера,
	 * @param Data_t:	тип элементов хранящихся в буфере, по умолчанию unsigned char
	 */
	template<size_t SIZE, class Data_t=unsigned char>
	class RingBuffer //: public Buffer<SIZE, Data_t>
	{
	public:
		/// Определяем псевдоним для индексов. int позволит использовать отрицательные индексы
		typedef int Index_t;  //size_t

		RingBuffer() : head_(0), tail_(0), length_(0), bourn_(SIZE)  {}
		RingBuffer(Index_t brn) : head_(0), tail_(0), length_(0), bourn_(brn)  {}
		//virtual ~RingBuffer() {}  // Для наследования.

	private:
		Data_t _data[SIZE];			/// буфер значений
		volatile Index_t head_ = 0; 	/// количество чтений, индекс головы
		volatile Index_t tail_ = 0; 	/// количество записей, индекс хвоста, индекс куда будет записан следующий элемент
		volatile Index_t length_;	/// количество элементов
		volatile Index_t bourn_; 	/// предельное наполнение //volatile Index_t actualSize /*= SIZE*/; /// SIZE определяет размер буффера в памяти, actualSize позволяет уменьшить текущий размер буффера
		
		class RingIndex {
			Index_t index;
			
		public:
			typedef int Index_t;

			RingIndex()             : index(0)        {}
			RingIndex(Index_t idx)  : index(idx)      {}
			//RingIndex(RingIndex ri) : index(ri.index) {}
			
			// Унарный плюс ничего не делает.
			friend inline const RingIndex& operator+( const RingIndex& ri ){
				return ri;
			}
			
			// Унарный минус
			friend inline const RingIndex operator-( const RingIndex& ri ){
				return RingIndex(-ri.index);
			}	
			
			// Префиксная версия возвращает значение после инкремента
			friend inline const RingIndex& operator++( RingIndex& ri ){
				ri.index = ++ri.index < (SIZE+1) ? ri.index : 0;
				return ri;
			}
			
			// Постфиксная версия возвращает значение до инкремента
			friend inline const RingIndex operator++( RingIndex& ri, int ){
				RingIndex oldValue(ri);
				ri.index = ++ri.index < (SIZE+1) ? ri.index : 0;
				return oldValue;
			}
			
			// Префиксная версия возвращает значение после декремента
			friend inline const RingIndex& operator--(RingIndex& ri) {
				ri.index = ++ri.index < (SIZE+1) ? ri.index : 0;
				return ri;
			}

			// Постфиксная версия возвращает значение до декремента
			friend inline const RingIndex operator--(RingIndex& ri, int) {
				RingIndex oldValue(ri);
				ri.index--;
				return oldValue;
			}
			
			// БИНАРНЫЕ ОПЕРАТОРЫ
			friend inline const RingIndex operator+( const RingIndex& left, const RingIndex& right ){
				return RingIndex( left.index + right.index );
			}
			
			friend inline const RingIndex operator+( const RingIndex& left, const Index_t& right ){
				return RingIndex( left.index + right );
			}

			friend inline RingIndex& operator+=(RingIndex& left, const RingIndex& right) {
				left.index += right.index;
				return left;
			}

			friend inline bool operator==(const RingIndex& left, const RingIndex& right) {
				return left.index == right.index;
			}
			
			inline Index_t getIndex() const {
				return this->index;
			}
			
			inline operator int() const {
				return this->index;
			}
		};

		
		/// Cледующий индекс, изменяет значение входного аргумента.
		inline Index_t nextIndex( volatile Index_t &idx) // volatile & было.
		{
			return idx = ++idx < bourn_ ? idx : 0;
		}
		
		/// Увеличение индекса на inc, изменяет значение входного параметра.
		volatile inline Index_t& incIndex( volatile Index_t &idx, Index_t inc ) // volatile & было.
		{
			idx += inc;
			if( idx >= bourn_ )
				idx -= bourn_;
			return idx;
		}
		
		/// Cледующий индекс, НЕ изменяет значение входного параметра.
		/*volatile*/ inline Index_t nextIndexOf( volatile Index_t idx) const 
		{
			return ++idx < bourn_ ? idx : 0;
		}
		
		/// Инкремент idx на inc, НЕ изменяет значение входного параметра.
		inline Index_t incIndexOf( Index_t idx, Index_t inc ) const 
		{
			Index_t ret = idx + inc;
			if( ret >= bourn_ )
				ret -= bourn_;
			return ret;
		}
		
	public:
		/** Запись в буфер одного элемента
		  * \return true если значение записано, false при переполнении
		  */
		inline bool push(Data_t value) {
			if( isFull() )	
				return false;
			_data[ tail_ ] = value;
			nextIndex(tail_);
			length_++;
			return true;
		}
		
		/** Запись в буфер массива элементов. Будет записано не более чем свободно.
		  * \return Количество вставленных элементов
		  */
		inline Index_t push( Data_t array[], Index_t length )
		{
			Index_t i=0;
			while( i<length && push(array[i]) )
				i++;
			return i;
		}
		
		/** Запись в буфер, выталкаивая первый элемент, если буффер заполнен.
		  * Цивильный вариант с небольшим оверхэдом. Опирается на готовые функции.
		  * \return true, если буффер полон и 1ый элемент был вытолкнут; false - если не полон.
		  */
		inline bool pushForce( const Data_t & value) {
			bool isful = isFull();
			if( isful )	
				pop();
			push(value);
			return isful;
		}
		
		/** Запись в буфер массива элементов, выталкивая все предыдущие если буффер заполнен.
		  * \return number of elements pushed
		  */
		inline Index_t pushForce( Data_t array[], Index_t length )
		{
			Index_t i=0;
			while( i<length ){
				pushForce( array[i++] );
			}
			return i;
		}
		
		/** Запись в буфер массива элементов, выталкивая все предыдущие если буффер заполнен.
		  * \return number of elements pushed
		  */
		template< size_t SIZ >
		inline Index_t pushForce( RingBuffer<SIZ,Data_t> inRB, Index_t nToPush = -1 )
		{
			if( nToPush == -1 || nToPush > inRB.length() )
				nToPush = inRB.length();
			Index_t i=0;
			while( i<nToPush ){
				pushForce( inRB[i++] );
			}
			return i;
		}
		
		/** Запись в буфер прореженного массива элементов, выталкивая все предыдущие если буффер заполнен.
		  * Прореживание (subsampling) ...
		  * \param 	array 	массив элементов
		  * \param 	length 	длина массива
		  * \param 	sub 	брать из массива каждый sub-ый элемент, например, каждый 2ой
		  * \return	number 	of elements pushed
		  */
		inline Index_t pushForceSubsampling( const Data_t array[], Index_t length, unsigned sub )
		{
			assert_amsg( array != 0);
			Index_t i=0, pushed=0;
			while( i<length ){
				pushForce( array[i] );
				i+=sub;
				pushed++;
			}
			return pushed;
		}
		
		/**
		 * 
		 */
		/*inline void setAllBuf( Data_t ){
			
		}*/
		
		/** Достать элемент, удаляя из буфера, и записать в value
		  * \return Возвращает true если значение прочитано (буфер не пуст), value изменяется.
		  */
		inline bool pop(Data_t &value) {
			if( isEmpty() ){
				//value = 0; //Data_t();
				return false;
			}
			value = _data[head_];
			nextIndex(head_);
			length_--;
			return true;
		}
		
		/** Достать и удалить из буфера
		 *  \return возвращает элемент или 0.
		 */
		inline Data_t pop() {
			Data_t value;
			if( isEmpty() ) {
				throw "Buffer is empty"; //value = 0;
			} else {
				value = _data[head_];
				nextIndex(head_);
				length_--;
			}
			return value;
		}
		
		/** Достать элементы, удаляя из буфера, и записать в массив
		  * \return Возвращает количество выплюнутых/записанных элементов
		  */
		inline Index_t pop(Data_t array[], size_t length) {
			if( isEmpty() ){
				return 0;
			}
			int i=0;
			while( i<length && pop(array[i++]) );
			return i;
		}
		
		/** Вытолкнуть элементы из буфера, и записать в RingBuffer rb
		  * \param rb куда записывать
		  * \return Возвращает количество вытолкнутых/вставленных комнонентов.
		  */
		template<size_t S, class T=unsigned char>
		inline Index_t pop( RingBuffer<S,T> &rb ){
			int i=0;
			while( !isEmpty() && rb.push(pop()) ){ i++; };
			return i;
		}
		
		///\todo оптимизировать
		/** 
		 * Достать и удалить из буфера len элементов
		 * \param len количество элементов
		 * \return возвращает количество вытолкнутых элементов; 0, если выталкивать нечего (пустой).
		 */
		inline Index_t pop(size_t len) {
			Index_t i=0;
			Data_t dummy;
			while( len>0 && pop(dummy) ){ 
				len--; i++;
			};
			return i;
		}
		
		/// возвращает первый элемент из буфера, не удаляя его
		inline Data_t& getFirst()/*const*/ {
			return operator[](0);
		}
		
		/// возвращает последний элемент из буфера, не удаляя его
		inline Data_t& getLast()/*const*/ {
			return operator[](length()-1);
		}
		
		/// эквивалент getLast()
		inline Data_t& peek() /*const*/ {
			return operator[](length()-1); 	/// элемент по [индексу]
		}
		
		/// 
		//inline Index_t copyTo( Data_t &d ) /*const*/ {
		//	abort_msg(__func__##" not implemented yet.\n");
		//	return 0;
		//}
		
		/// Copy elements `len` elements from buffer to `array`.
		///\param offset  the start index of ring buffer
		inline Index_t copyToArray( Data_t array[], Index_t len, Index_t offset=0 ) /*const*/ {
			int i=0, j; if( -length()<=offset && offset<length() )  j=offset; else j=0;
			while( i<len && j<length() ){
				array[i] = operator[](j);
				i++; j++;
			}
			return i;
		}
		
		/// Copy elements last `len` elements from buffer to `array`.
		inline Index_t copyLastToArray( Data_t array[], Index_t len ) /*const*/ {
			Index_t i=0, j = length() > len ? length()-len : 0;
			while( i<len && j<length() ){
				array[i] = operator[](j);
				i++; j++;
			}
			return i;
		}
		
		/// Обрезать буфер.
		inline Index_t trim( Index_t fromBegin/*, Index_t fromEnd=0*/ ){
			if( fromBegin > length() )
				fromBegin = length();
			incIndex(head_, fromBegin);
			length_ -= fromBegin;
			return length_;
		}
		
		/** Возвращает элемент (ссылку) по индексу
		 *  Отрицательный индекс означает отсчёт с хвоста, [-1] это последний, [-2] предпоследний */ 
		inline Data_t& operator[](Index_t i) //const
		{
			//if(!(-length() <= i && i < length()))
			//	printf("if(-length() <= i && i < length()) i=%d, len=%d\n", i, length_);
			assert_amsg( length_!=0/*!isEmpty()*/ ); assert_amsg( (-length()) <= i && i < length() );
			/*if( isEmpty() || length() <= i || i < -length() ){
				return *(Data_t*)NULL; 
				throw std::out_of_range("Index out of bounds"); //throw IndexOutOfBoundsException;
			}*/
			if( i<0 )
				i += length();
			return _data[ incIndexOf(head_, i) ];
		}
		
		/**	Возвращает const элемент (значение элемента) по индексу. const this
		  *	Отрицательный индекс означает отсчёт с хвоста, [-1] это последний, [-2] предпоследний */
		/*inline const Data_t operator[] (Index_t i) const 
		{
			if( isEmpty() || i >= length() || i < -length() )
				return Data_t();
			else if( i<0 )
				i += length();
			return _data[ incIndexOf(head_, i) ];
		}*/
		
		/// Пуст ли буфер
		inline bool isEmpty() const 
		{
			return length_ == 0; //return tail_ == head_;
		}
		
		/// Полон ли буфер
		inline bool isFull() const 
		{
			return length_ == bourn_; //return length_ == SIZE; //return nextIndexOf(tail_) == head_;
		}
		
		/// Очистить буфер
		inline void clear() 
		{
			head_ = tail_ = length_ = 0;
		}
		
		/// Количество элементов в буфере
		inline Index_t length() const {
			return length_; //return (tail_ >= head_) ? (tail_ - head_) : (SIZE+1 - head_ + tail_);
		}
		
		/// Размер буфера
		inline size_t size() {
			return SIZE;
		}
		
		/// Получить предельный размер для записи. Удобно при динамическом изменении длины кольцевого буфера.
		/// Например, чтобы укоротить/удлиннить фильтр во время выполнения.
		inline size_t bourn() {
			return bourn_;
		}
		
		/// Установить предельный размер для записи. Удобно при динамическом изменении длины кольцевого буфера.
		/// Не может привышать SIZE.
		/// При уменьшении, Буфер выплюнет в пустоту лишнее (более старое).
		/// \return установленное значение
		inline size_t bourn(size_t b) 
		{
			if( b <= size() )  bourn_ = b;
			else bourn_ = size();
			// выбросить лишние
			if( length() > bourn_ ) 
				trim( length() - bourn_ );
			return bourn_;
		}
		

	};

} // namespace Sledge

namespace slg = Sledge;


#endif /* _Sledge_RingBuffer_hpp_ */
